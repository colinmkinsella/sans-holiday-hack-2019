FROM thomasweise/docker-texlive-full

# Installing Emerald Font
RUN mkdir `kpsewhich --var-value=TEXMFHOME` &&\
    cd `kpsewhich --var-value=TEXMFHOME` &&\
    wget http://mirror.ctan.org/fonts/emerald.zip &&\
    unzip emerald.zip &&\
    cp -r emerald/. . && rm -rf emerald/ &&\
    rm emerald.zip &&\
    updmap-sys --enable Map emerald.map &&\
    texhash
