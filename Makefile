all: sansholidayhack2019writeup.pdf

sansholidayhack2019writeup.pdf: *.tex
	pdflatex sansholidayhack2019writeup
	pdflatex sansholidayhack2019writeup

clean:
	rm -f *.aux
	rm -f *.bbl
	rm -f *.blg
	rm -f *.log
	rm -f *.pdf
