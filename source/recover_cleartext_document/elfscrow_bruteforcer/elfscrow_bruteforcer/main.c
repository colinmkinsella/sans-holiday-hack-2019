#include <Windows.h>

#include <malloc.h>
#include <stdio.h>
#include <wincrypt.h>

#define DEC06_2019_7PM_UTC 1575658800
#define DEC06_2019_9PM_UTC 1575666000
#define BUFFER_LENGTH 16
#define KEY_SIZE 8

void print_encryption_key(byte* s, int length)
{
	byte* buffer = NULL;
	byte* p = s;

	// one hex char is two characters
	length = length * 2;

	// padding the buffer since the hex conversion likes to over extend...
	buffer = (unsigned char*)calloc(length * 2, sizeof(byte));
	if( NULL == buffer)
	{
		goto done;
	}

	for( int x = 0; x < length; x+=2 )
	{
		sprintf_s(((char*)buffer) + x, length , "%02x", *p);
		p++;
	}

	printf("encryption key: %s", buffer);

done:
	if (NULL != buffer)
	{
		free(buffer);
		buffer = NULL;
	}
}


int super_secure_random(int* seed)
{
	if( NULL == seed )
	{
		return 0;
	}

	// Linear Congruential Generator (LCG)
	(*seed) = (*seed) * 0x343fd+0x269EC3; 
	return ((*seed) >> 0x10) & 0x7FFF;
}

HRESULT Decrypt(unsigned char* key, byte* data, DWORD* dataLength)
{
	BYTE keyblob[] = {
		0x08,0x02,0x00,0x00,0x01,0x66,0x00,0x00, // BLOB header 
		0x08,0x00,0x00,0x00,                     // key length, in bytes
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00  // DES key with parity
    };
	HCRYPTKEY hKey = 0;
	HCRYPTPROV hProv = 0;
	HRESULT hResult = ERROR_SUCCESS;
	unsigned int offset = 12;
	
	
	if( NULL == key )
	{
		goto done;
	}

	for( unsigned int x = 0; x < KEY_SIZE; x++ )
	{
		keyblob[x + offset] = key[x];
	}

	if( !CryptAcquireContextA(&hProv, NULL, MS_ENHANCED_PROV, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT) )
	{
		hResult = GetLastError();
		goto done;
	}

	if( !CryptImportKey(hProv, keyblob, sizeof(keyblob), 0, CRYPT_EXPORTABLE, &hKey) )
	{
		hResult = GetLastError();
		goto done;
	}

	if( !CryptDecrypt(hKey, 0, TRUE, 0, data, dataLength) )
	{
		hResult = GetLastError();
		goto done;
	}
	
done:
	if( 0 != hKey)
	{
		CryptDestroyKey(hKey);
	}

	if( 0 != hProv )
	{
		CryptReleaseContext(hProv, 0);
	}
	return hResult;
}

HRESULT main(int argc, char *argv[]) 
{
	BOOL found = FALSE;
	byte* buffer = NULL;
	byte* encryptedBuffer = NULL;
	byte key[KEY_SIZE] = { 0 };
	char* inpath = NULL;
	char* outpath = NULL;
	DWORD bytesRead = 0;
	DWORD bytesWritten = 0;
	DWORD encryptedBufferLength = 0;
	DWORD fileSize = 0;
	HANDLE hDecryptedFile = NULL;
	HANDLE hEncryptedFile = NULL;
	HRESULT hResult = ERROR_SUCCESS;
	int seed = 0;

	if( argc != 3 )
	{
		printf("usage: %s <encrypted filepath> <decrypted filepath>", argv[0]);
		goto done;
	}

	inpath = argv[1];
	outpath = argv[2];

	buffer = (byte*)calloc(BUFFER_LENGTH, 1);

	hEncryptedFile = CreateFile(inpath, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if( NULL == hEncryptedFile)
	{
		hResult = GetLastError();
		goto done;
	}

	hDecryptedFile = CreateFile(outpath, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if( NULL == hDecryptedFile)
	{
		hResult = GetLastError();
		goto done;
	}

	encryptedBufferLength = GetFileSize(hEncryptedFile, NULL);
	if( INVALID_FILE_SIZE == encryptedBufferLength )
	{
		hResult = GetLastError();
		goto done;
	}
	
	encryptedBuffer = (byte*)calloc(encryptedBufferLength, 1);
	if( !ReadFile(hEncryptedFile, encryptedBuffer, encryptedBufferLength, &encryptedBufferLength, NULL) )
	{
		hResult = GetLastError();
		goto done;
	}

	// looping through all the possible seed values
	for( int x = DEC06_2019_7PM_UTC; x <= DEC06_2019_9PM_UTC; x++)
	{
		seed = x;

		for (unsigned int y = 0; y < KEY_SIZE; y++)
		{
			key[y] = super_secure_random(&seed) & 0xff;
		}

		memcpy(buffer, encryptedBuffer, BUFFER_LENGTH);
		bytesRead = BUFFER_LENGTH;
		
		Decrypt(key, buffer, &bytesRead);
		if (memcmp(buffer, "%PDF", 4) == 0)
		{
			found = TRUE;
			break;
		}
	}

	if( FALSE == found )
	{
		goto done;
	}

	print_encryption_key(key, sizeof(key));

	bytesRead = encryptedBufferLength;
	hResult = Decrypt(key, encryptedBuffer, &bytesRead);
	if( S_OK != hResult )
	{
		goto done;
	}

	if( !WriteFile(hDecryptedFile, encryptedBuffer, bytesRead, &bytesWritten, NULL) )
	{
		hResult = GetLastError();
		goto done;
	}

done:	
	if (NULL != hDecryptedFile)
	{
		CloseHandle(hDecryptedFile);
	}

	if (NULL != hEncryptedFile)
	{
		CloseHandle(hEncryptedFile);
	}

	if (NULL != buffer)
	{
		free(buffer);
	}

	if (NULL != encryptedBuffer)
	{
		free(encryptedBuffer);
	}

	return hResult;
}